# ReviewData


0. Plik data.csv nie wyglada najlepiej, ale zakładam że nie podlega review.
1. Zła nazwa pliku w ```Main()```, "dataa.csv" zamiast "data.csv.
2. Nieużywane usingi można usunać.
3. Metoda ```ImportAndPrintData``` troche mi nie pasuje - robi dwie czynnosci na raz, wczytuje i wypisuje dane, rozbiłbym to na ```ImportData()``` i ```PrintData()```.
4. Parametr ```printData``` nic nie robi - do usuniecia.
5. ```Console.ReadLine()``` przeniósłbym do ```Main()``` - tak aby ```PrintData()``` zajmowało się tylko wypisaniem danych, a nie również zatrzymywaniem programu. 
6. ```ImportedObjects``` - dopisałbym dla czytelności że jest ```private```, oraz zmienil nazwe na ```_importedObjects```, to nie jest jakiś blad ale przydałoby się stosować inną konwencje nazewniczą dla publicznych i prywatnych zmiennych.
7. Koncepcja ```ImportedObject``` i ```ImportedObjectBaseClass``` jest moim zdaniem dziwna. Klasy możnaby przenieśc do innych plików, albo umieścić w klasie ```DataReader``` jako klasy zagnieżdzone. Zastanowić się czy ta klasa bazowa jest w ogole potrzebna? Jeśli tak to może powinna być abstrakcyjna?
8. ```ImportedObject.Name```? To chyba błąd - ```Name``` jest już w klasie bazowej, wiec tam jest niepotrzebne.
9. Propertisy w tych dwóch klasach raz maja gettery i settery, raz nie, raz są robione w jednej lini, raz po enterze - zmienić tak, aby była jedna konwencja. 
10. ```ImportedObject.NumberOfChildren``` powinno być typem liczby całkowitej, np int'em.
11. ```ImportedObject.IsNullable``` - możnaby się zastanowić czy nie dałoby sie zrobić jako ```bool```. A ```Type``` i ```ParentType``` jako enum.
12. Nie rozumiem dlaczego ```ImportedObjects``` przy tworzeniu ma dodawany obiekt? Nie ddawałbym go. Możnaby też rozważyć czy ta lista powinna być tworzona w tej metodzie.
13. ```StreamReader``` użyłbym w using'u. tak aby zwolnić zasób po zakończeniu jego pracy.
14. Pętla ```for``` ma zły znak przy warunku ```i <= importedLines.Count```, powinno być```i < importedLines.Count```, można też zaczac od ```i = 1```, bo pierwszy wiersz w pliku to nagłówek.
15. Import wyrzuca bład jeśli linia nie ma odpowiedniej ilości danych po rozlączeniu na ";". Dodałbym warunek, aby dane importowac tylko jeśli posiadamy 7 elementów w tablicy ```values```;
16. Niepotrzebne rzutowanie ```((List<ImportedObject>)ImportedObjects```, typ obiektu można zmienić z ```IEnumerable``` na ```List```. 
17. ```Trim()``` usuwa spacje i znaki nowej lini, więc można usunąc tamte ```Replace()```. Nie ma też potrzeby wykonywać tego na każdym stringu, przenióslbym to wyżej do importu i wykonał ```Trim()``` na ```importedLine```. wtedy ```values[0]``` będzie z funkcją ```ToUpper()```.
18. Wywolanie ```ToArray()``` w pętli po to, aby odwolać się do elementu tablicy po indeksie nie ma sensu, mozna zmienić petle ```for``` na ```foreach```.
19. ```importedObject.NumberOfChildren = 1 + importedObject.NumberOfChildren``` można zastąpić ```importedObject.NumberOfChildren++```- moim zdaniem tak jest czytelniej
20. Petla przypisującą ```NumberOfChildren``` jest dość złożona, potem przy wypisaniu danych wykorzystujemy kolejny raz taką zagniezdzona pętle...
 Zmieniłbym ```NumberOfChildren``` na liste obiektow "dzieci" i wykorzystał to w funkcji ```PrintData()``` (można pomyslec o oparciu jej o rekurencje). W tym celu ```ImportedObjects``` zmieniłbym na zmieną lokalną w metodzie ImportData i zastąpił ją zmienną ```_importedDatabases```, gdzie przechowywałbym tylko bazy danych.

## Uwagi końcowe

Program posiada za duzo błedów aby omówic wszystko podczas jednego review. Punkty które wypisałem wyżej moga być niezrozumiałe bo np. liste ```ImportedObjects``` edytowałem z 3 razy, tylko po to aby w ostatnim punkcie ją zastapić czymś innym. Podobnie było z ```NumberOfChildren```. Gdybym rzeczywiscie musiał robić takie review to skontaktowałbym się z osoba która to pisała i tłumaczył każdy problem na bieżąco, lub rozbił całość na kilka mniejszych poprawek - tzn najpierw naprawa kodu aby program działał, a potem jego optymalizacja: stworzenie enum'a, zastapienie ```NumberOfChildren``` itp.