﻿namespace ConsoleApp
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    public class DataReader
    {
        private List<ImportedObject> _importedDatabases = new List<ImportedObject>();

        public void ImportData(string fileToImport)
        {
            List<ImportedObject> importedObjects = new List<ImportedObject>();

            using (var streamReader = new StreamReader(fileToImport))
            {
                var importedLines = new List<string>();
                while (!streamReader.EndOfStream)
                {
                    var line = streamReader.ReadLine();
                    importedLines.Add(line);
                }

                for (int i = 1; i < importedLines.Count; i++)
                {
                    var importedLine = importedLines[i];
                    var values = importedLine.Trim().Split(';');
                    if (values.Length >= 7)
                    {

                        DataType dataType;
                        DataType parentDataType;
                        if (!Enum.TryParse(values[0], true, out dataType))
                            continue;
                        if (!Enum.TryParse(values[4], true, out parentDataType) && dataType != DataType.DATABASE)
                            continue;

                        var importedObject = new ImportedObject();
                        importedObject.Type = dataType;
                        importedObject.Name = values[1];
                        importedObject.Schema = values[2];
                        importedObject.ParentName = values[3];
                        importedObject.ParentType = parentDataType;
                        importedObject.DataType = values[5];
                        importedObject.IsNullable = values[6] != "1" && dataType == DataType.COLUMN;
                        importedObjects.Add(importedObject);
                    }
                }
            }

            // assign number of children
            foreach (var importedObject in importedObjects)
            {
                if (importedObject.Type == DataType.DATABASE)
                    _importedDatabases.Add(importedObject);

                foreach (var impObj in importedObjects)
                {
                    if (impObj.ParentType == importedObject.Type)
                    {
                        if (impObj.ParentName == importedObject.Name)
                        {
                            importedObject.ImportedChildren.Add(impObj);
                        }
                    }
                }
            }
        }

        public void PrintData()
        {
            foreach (var database in _importedDatabases)
            {
                if (database.Type == DataType.DATABASE)
                {
                    Console.WriteLine($"Database '{database.Name}' ({database.ImportedChildren.Count} tables)");

                    // print all database's tables
                    foreach (var table in database.ImportedChildren)
                    {
                        if (table.ParentType == database.Type)
                        {
                            if (table.ParentName == database.Name)
                            {
                                Console.WriteLine($"\tTable '{table.Schema}.{table.Name}' ({table.ImportedChildren.Count} columns)");

                                // print all table's columns
                                foreach (var column in table.ImportedChildren)
                                {
                                    if (column.ParentType == table.Type)
                                    {
                                        if (column.ParentName == table.Name)
                                        {
                                            Console.WriteLine($"\t\tColumn '{column.Name}' with {column.DataType} data type {(column.IsNullable ? "accepts nulls" : "with no nulls")}");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private enum DataType { DATABASE, TABLE, COLUMN}
        private class ImportedObject : ImportedObjectBaseClass
        {
            public string Schema { get; set; }

            public string ParentName { get; set; }
            public DataType? ParentType { get; set; }

            public string DataType { get; set; }
            public bool IsNullable { get; set; }

            public List<ImportedObject> ImportedChildren { get; set; } = new List<ImportedObject>();

        }

        private abstract class ImportedObjectBaseClass
        {
            public string Name { get; set; }
            public DataType Type { get; set; }
        }
    }
}
